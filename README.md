## [VulnHub] Proteus VM Writeup

Here is a solution to [Proteus VM from VulnHub](https://www.vulnhub.com/entry/proteus-1,193/).

## Malware analysis tool

While scanning the Proteus VM, I noticed a WEB service hosted with Apache. Since I haven't found something much interesting, I decided to deep dive into this  "malware analysis tool".

After couple of minutes, I first found a way to upload a PHP file, but nothing allowed me to include it / run it... Then I remembered this: "This VM was written in a manner that does not require `wget http://exploit; gcc exploit`." so I had thought that if we gain admin rights (and an account ^^) on this application, we'll be able to access new features like running binary.

So let's upload a netcat i386-compiled binary (our future reverse shell) and look around for vulnerabilities that can be exploited to get an admin account.

## Session Cookie

After couple of tests like SQLi, I finally get a closer look at cookies and noticed a specific format (guess: `{TIMESTAMP}%7C{BASE64_DATA}%7C{SIG}`).

![1_retrieve_cookie][]

A quick search on [Duckduckgo](https://duckduckgo.com/?q=session+cookie+middleware+PHP&t=ffsb&ia=qa) highlights some similarities with the Slim Framework's Session Cookie middleware! Looking for CVE or known flaws, I found [CVE-2015-2171](http://seclists.org/fulldisclosure/2015/Mar/16) which allows to perform code injection using an `unserialize()` function call.

Since I don't have much information to know if this service relies on Slim Framework and that I haven't found a working exploit, I decided to build a Slim Framework lab in order to decode and forge a new cookie.

### Building the lab

Get Slim Framework sources:

```
git clone https://github.com/slimphp/Slim/ /tmp/slim-src
pushd /tmp/slim-src/
```

Identify releases prior [CVE-2015-2171 patch](https://github.com/slimphp/Slim/commit/9fa651474eb4d3bb0ce40dd5a55c51bb861c2658):

```
git tag --contains 9fa651474eb4d3bb0ce40dd5a55c51bb861c2658 # get tag containing CVE-2015-2171 fix -> v2.6.0 -> https://github.com/slimphp/Slim/releases/tag/2.6.0
```

First try with `v2.5.0` returned a URL-encoded PHP `serialize()` object, so I checked back to history and found [Removes encryption and decryption, adds some error handling](https://github.com/slimphp/Slim/commit/f16cfd6e96521fb3a64a12a9a7d26ba364578d32):

```
git tag --contains f16cfd6e96521fb3a64a12a9a7d26ba364578d32 # v2.3.2 -> https://github.com/slimphp/Slim/releases/tag/2.3.2
popd
```

Create a new project based on slim v2.3.1 according to [documentation](http://docs.slimframework.com/):

```bash
pushd /var/www/html/
composer create-project slim/slim-skeleton:2 slim # https://github.com/slimphp/Slim-Skeleton
cd slim/
composer remove slim/views # remove dependencies from slim v3
composer require slim/slim:2.3.1
cat <<-'EOF' >public/index.php
<?php
require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\Slim();

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

$app->run();

EOF
mkdir src/
cat <<-'EOF' >src/middleware.php # http://docs.slimframework.com/sessions/cookies/
<?php
$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session'
)));

EOF
cat <<-'EOF' >src/routes.php
<?php
// Routes

$app->get('/', function () {});

EOF
```

[Browsing the application](http://localhost/) returns back a cookie with the same format as `proteus_session` (final thought: `{TIMESTAMP}%7C{BASE64_DATA}%7C{SHA1_SIG}`).

So, let's decode Proteus session content from `proteus_session` cookie.

## `proteus_session` decoding

Apply a patch to middleware to make our application decode our provided cookie:

```
wget https://git.bmoine.fr/vulnhub-proteus-writeup/raw/master/session_cookie_dump.patch -O session_cookie_dump.patch
patch -p0 -i session_cookie_dump.patch
```

Test with the current `proteus_session` cookie:

![2_decode_cookie][]

### Admin impersonation

After couple of tests, I finally be able to identify a matching cookie format to get admin rights with admin impersonation: `a:2:{s:5:"admin";b:1;s:4:"name";s:7:"Creased";}`

Forge a new cookie with this payload:

![3_forge_new_cookie][]

And, apply it to current session:

![4_access_samples_w_admin_rights][]

As expected, we have access to a admin-only `delete` feature that is based on a Base64 encoding of our sample name. Assuming it's a simple `rm` applied to the specified file, I tried to perform command injection:

![5_prepare_command_injection_w_delete][]

![6_command_injection_w_delete][]

It works! Final steps were easy:

![7_connect_to_rshell_and_dig][]
![8_get_root][]
![9_get_flag][]

## Conclusion

Thanks to [ivanvza](https://twitter.com/viljoenivan) for this amazing VM, really enjoyable.

And a really big thanks to VulnHub team for hosting these challenges, you're awesome!

<!-- screenshots -->

 [1_retrieve_cookie]: illustrations/1_retrieve_cookie.png
 [2_decode_cookie]: illustrations/2_decode_cookie.png
 [3_forge_new_cookie]: illustrations/3_forge_new_cookie.png
 [4_access_samples_w_admin_rights]: illustrations/4_access_samples_w_admin_rights.png
 [5_prepare_command_injection_w_delete]: illustrations/5_prepare_command_injection_w_delete.png
 [6_command_injection_w_delete]: illustrations/6_command_injection_w_delete.png
 [7_connect_to_rshell_and_dig]: illustrations/7_connect_to_rshell_and_dig.png
 [8_get_root]: illustrations/8_get_root.png
 [9_get_flag]: illustrations/9_get_flag.png
